<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Name : ". $sheep->name."<br>"; // "shaun"
echo "Legs : ". $sheep->legs. "<br>" ;// 4
echo "Cold Blooded : ". $sheep->cold_blooded. "<br><br>"; // "no"


$kodok = new Frog("buduk");
echo "Name : ". $kodok->name."<br>";
echo "legs : ". $kodok->legs."<br>";
echo "Cold Blooded : ". $kodok->cold_blooded."<br>";
echo "Jump : ". $kodok->jump(). "<br><br>" ; // "hop hop"


$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->name. "<br>";
echo "legs : ". $sungokong->legs. "<br>";
echo "Cold Blooded : ". $sungokong->cold_blooded."<br>";
echo "Yell : ". $sungokong->yell(); // "Auooo"
